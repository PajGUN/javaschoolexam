package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        // finding the matrix size
        int height = 0;
        int width = 0;
        int count = inputNumbers.size();
        for (int i = 1; i <= inputNumbers.size(); i++){
            if (count-i == 0) {
                height = i;
                width = i * 2 - 1;
                break;
            }
            else {
                count -= i;
                if (count < 0) throw new CannotBuildPyramidException();
            }
        }

        // making matrix and fill with zeros
        int[][] pyramid = new int[height][width];
        for (int[] ints : pyramid) {
            Arrays.fill(ints,0);
        }

        // i think it's a good idea do not change original list
        List<Integer> newList = new ArrayList<>(inputNumbers);
        Collections.sort(newList);

        //
        int countNumsInRow = 1;
        int supportUnit = 0;
        for (int i = 0; i < pyramid.length; i++) {
            countNumsInRow = i + 1;
            for (int j = 0; j < pyramid[0].length; j++){
                if (i == 0) {
                    supportUnit = pyramid[0].length / 2;
                    pyramid[i][supportUnit] = newList.get(0);
                    newList.remove(0);
                    break;
                }
                if (j == supportUnit) {
                    int tmp = supportUnit;
                    while (countNumsInRow != 0){
                        pyramid[i][tmp] = newList.get(0);
                        newList.remove(0);
                        countNumsInRow--;
                        tmp+=2;
                    }
                    break;
                }
            }
            supportUnit--;
        }
        return pyramid;
    }
}
