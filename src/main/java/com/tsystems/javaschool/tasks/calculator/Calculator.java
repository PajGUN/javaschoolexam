package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        try {
            //double operators test
            char[] op = statement.toCharArray();
            for (int i = 0; i<statement.length()-1; i++){
                if ((op[i] == '/' || op[i] == '*' || op[i] == '-' || op[i] == '+') &&
                        (op[i+1] == '/' || op[i+1] == '*' || op[i+1] == '-' || op[i+1] == '+')){
                    return null;
                }
            }

            if (statement.contains("(")){
                int firstIndex = statement.indexOf("(");
                int lastIndex = statement.lastIndexOf(")");
                String rec = evaluate(statement.substring(firstIndex+1,lastIndex));
                statement = statement.replace(statement.substring(firstIndex,lastIndex+1),rec);
            }

            String[] arrayString = statement.split("[*/+-]");
            LinkedList<String> linkedList = new LinkedList<>();
            String operators = statement.trim().replaceAll("[\\d.]","");
            linkedList.add(arrayString[0]);

            for (int i = 0; i < operators.length(); i++){
                linkedList.add(String.valueOf(operators.charAt(i)));
                linkedList.add(arrayString[i+1]);
            }
            if (linkedList.contains("")){
                linkedList.remove("");
                int index = linkedList.indexOf("-");
                linkedList.set(index, linkedList.get(index)+linkedList.get(index+1));
                linkedList.remove(index+1);
            }

            while (linkedList.contains("/") || linkedList.contains("*")){
                int index = -1;
                for (int i = 0; i < linkedList.size(); i++) {
                    String tmp = linkedList.get(i);
                    if (tmp.equals("/") || tmp.equals("*")){
                        index = i;
                        break;
                    }
                }
                if (index != -1){
                    double tmp;
                    if (linkedList.get(index).equals("/")){
                        tmp = Double.parseDouble(linkedList.get(index-1)) / Double.parseDouble(linkedList.get(index+1));
                    } else {
                        tmp = Double.parseDouble(linkedList.get(index-1)) * Double.parseDouble(linkedList.get(index+1));
                    }
                    linkedList.set(index, String.valueOf(tmp));
                    linkedList.remove(index+1);
                    linkedList.remove(index-1);
                }
            }

            while (linkedList.contains("-") || linkedList.contains("+")){
                int index = -1;
                for (int i = 0; i < linkedList.size(); i++) {
                    String tmp = linkedList.get(i);
                    if (tmp.equals("-") || tmp.equals("+")){
                        index = i;
                        break;
                    }
                }
                if (index != -1){
                    double tmp;
                    if (linkedList.get(index).equals("-")){
                        tmp = Double.parseDouble(linkedList.get(index-1)) - Double.parseDouble(linkedList.get(index+1));
                    } else {
                        tmp = Double.parseDouble(linkedList.get(index-1)) + Double.parseDouble(linkedList.get(index+1));
                    }
                    linkedList.set(index, String.valueOf(tmp));
                    linkedList.remove(index+1);
                    linkedList.remove(index-1);
                }
            }

            String result = linkedList.get(0);
            if (result.endsWith(".0")){
                result = result.replace(".0","");
            }
            else if (result.equals("Infinity")) result = null;

            return result;
        } catch (Exception ex){
            return null;
        }
    }
}
